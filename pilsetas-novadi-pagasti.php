<?php

$pilsetas = array(
	'Daugavpils',
	'Jēkabpils',
	'Jelgava',
	'Jūrmala',
	'Liepāja',
	'Rēzekne',
	'Rīga',
	'Valmiera',
	'Ventspils'
);

$novadi = array(
	'Ādažu novads' => array(),
	'Aglonas novads' => array(
		'Aglonas pagasts',
		'Grāveru pagasts',
		'Kastuļinas pagasts',
		'Šķeltovas pagasts'
	),
	'Aizkraukles novads' => array(
		'Aizkraukles pagasts',
		'Aizkraukles pilsēta'
	),
	'Aizputes novads' => array(
		'Aizputes pagasts',
		'Aizputes pilsēta',
		'Cīravas pagasts',
		'Kalvenes pagasts',
		'Kazdangas pagasts',
		'Lažas pagasts'
	),
	'Aknīstes novads' => array(
		'Aknīstes pagasts',
		'Aknīstes pilsēta',
		'Asares pagasts',
		'Gārsenes pagasts'
	),
	'Alojas novads' => array(
		'Alojas pagasts',
		'Alojas pilsēta',
		'Braslavas pagasts',
		'Brīvzemnieku pagasts',
		'Staiceles pagasts',
		'Staiceles pilsēta'
	),
	'Alsungas novads' => array(),
	'Alūksnes novads' => array(
		'Alsviķu pagasts',
		'Alūksnes pilsēta',
		'Annas pagasts',
		'Ilzenes pagasts',
		'Jaunalūksnes pagasts',
		'Jaunannas pagasts',
		'Jaunlaicenes pagasts',
		'Kalncempju pagasts',
		'Liepnas pagasts',
		'Malienas pagasts',
		'Mālupes pagasts',
		'Mārkalnes pagasts',
		'Pededzes pagasts',
		'Veclaicenes pagasts',
		'Zeltiņu pagasts',
		'Ziemera pagasts'
	),
	'Amatas novads' => array(
		'Amatas pagasts',
		'Drabešu pagasts',
		'Nītaures pagasts',
		'Skujenes pagasts',
		'Zaubes pagasts'
	),
	'Apes novads' => array(
		'Apes pagasts',
		'Apes pilsēta',
		'Gaujienas pagasts',
		'Trapenes pagasts',
		'Virešu pagasts'
	),
	'Auces novads' => array(
		'Auces pilsēta',
		'Bēnes pagasts',
		'Īles pagasts',
		'Lielauces pagasts',
		'Ukru pagasts',
		'Vecauces pagasts',
		'Vītiņu pagasts'
	),
	'Babītes novads' => array(
		'Babītes pagasts',
		'Salas pagasts'
	),
	'Baldones novads' => array(
		'Baldones pagasts',
		'Baldones pilsēta'
	),
	'Baltinavas novads' => array(),
	'Balvu novads' => array(
		'Balvu pagasts',
		'Balvu pilsēta',
		'Bērzkalnes pagasts',
		'Bērzpils pagasts',
		'Briežuciema pagasts',
		'Krišjāņu pagasts',
		'Kubulu pagasts',
		'Lazdulejas pagasts',
		'Tilžas pagasts',
		'Vectilžas pagasts',
		'Vīksnas pagasts'
	),
	'Bauskas novads' => array(
		'Bauskas pilsēta',
		'Brunavas pagasts',
		'Ceraukstes pagasts',
		'Codes pagasts',
		'Dāviņu pagasts',
		'Gailīšu pagasts',
		'Īslīces pagasts',
		'Mežotnes pagasts',
		'Vecsaules pagasts'
	),
	'Beverīnas novads' => array(
		'Brenguļu pagasts',
		'Kauguru pagasts',
		'Trikātas pagasts'
	),
	'Brocēnu novads' => array(
		'Blīdenes pagasts',
		'Brocēnu pilsēta',
		'Cieceres pagasts',
		'Gaiķu pagasts',
		'Remtes pagasts'
	),
	'Burtnieku novads' => array(
		'Burtnieku pagasts',
		'Ēveles pagasts',
		'Matīšu pagasts',
		'Rencēnu pagasts',
		'Valmieras pagasts',
		'Vecates pagasts'
	),
	'Carnikavas novads' => array(),
	'Cēsu novads' => array(
		'Cēsu pilsēta',
		'Vaives pagasts'
	),
	'Cesvaines novads' => array(
		'Cesvaines pagasts',
		'Cesvaines pilsēta'
	),
	'Ciblas novads' => array(
		'Blontu pagasts',
		'Ciblas pagasts',
		'Līdumnieku pagasts',
		'Pušmucovas pagasts',
		'Zvirgzdenes pagasts'
	),
	'Dagdas novads' => array(
		'Andrupenes pagasts',
		'Andzeļu pagasts',
		'Asūnes pagasts',
		'Bērziņu pagasts',
		'Dagdas pagasts',
		'Dagdas pilsēta',
		'Ezernieku pagasts',
		'Konstantinovas pagasts',
		'Ķepovas pagasts',
		'Svariņu pagasts',
		'Šķaunes pagasts'
	),
	'Daugavpils novads' => array(
		'Ambeļu pagasts',
		'Biķernieku pagasts',
		'Demenes pagasts',
		'Dubnas pagasts',
		'Kalkūnes pagasts',
		'Kalupes pagasts',
		'Laucesas pagasts',
		'Līksnas pagasts',
		'Maļinovas pagasts',
		'Medumu pagasts',
		'Naujenes pagasts',
		'Nīcgales pagasts',
		'Salienas pagasts',
		'Skrudalienas pagasts',
		'Sventes pagasts',
		'Tabores pagasts',
		'Vaboles pagasts',
		'Vecsalienas pagasts',
		'Višķu pagasts'
	),
	'Dobeles novads' => array(
		'Annenieku pagasts',
		'Auru pagasts',
		'Bērzes pagasts',
		'Bikstu pagasts',
		'Dobeles pagasts',
		'Dobeles pilsēta',
		'Jaunbērzes pagasts',
		'Krimūnu pagasts',
		'Naudītes pagasts',
		'Penkules pagasts',
		'Zebrenes pagasts'
	),
	'Dundagas novads' => array(
		'Dundagas pagasts',
		'Kolkas pagasts'
	),
	'Durbes novads' => array(
		'Dunalkas pagasts',
		'Durbes pagasts',
		'Durbes pilsēta',
		'Tadaiķu pagasts',
		'Vecpils pagasts'
	),
	'Engures novads' => array(
		'Engures pagasts',
		'Lapmežciema pagasts',
		'Smārdes pagasts'
	),
	'Ērgļu novads' => array(
		'Ērgļu pagasts',
		'Jumurdas pagasts',
		'Sausnējas pagasts'
	),
	'Garkalnes novads' => array(),
	'Grobiņas novads' => array(
		'Bārtas pagasts',
		'Gaviezes pagasts',
		'Grobiņas pagasts',
		'Grobiņas pilsēta',
		'Medzes pagasts'
	),
	'Gulbenes novads' => array(
		'Beļavas pagasts',
		'Daukstu pagasts',
		'Druvienas pagasts',
		'Galgauskas pagasts',
		'Gulbenes pilsēta',
		'Jaungulbenes pagasts',
		'Lejasciema pagasts',
		'Līgo pagasts',
		'Litenes pagasts',
		'Lizuma pagasts',
		'Rankas pagasts',
		'Stāmerienas pagasts',
		'Stradu pagasts',
		'Tirzas pagasts'
	),
	'Iecavas novads' => array(),
	'Ikšķiles novads' => array(
		'Ikšķiles pilsēta',
		'Tīnūžu pagasts'
	),
	'Ilūkstes novads' => array(
		'Bebrenes pagasts',
		'Dvietes pagasts',
		'Eglaines pagasts',
		'Ilūkstes pilsēta',
		'Pilskalnes pagasts',
		'Prodes pagasts',
		'Subates pilsēta',
		'Šēderes pagasts'
	),
	'Inčukalna novads' => array(
		'Inčukalna pagasts',
		'Vangažu pilsēta'
	),
	'Jaunjelgavas novads' => array(
		'Daudzeses pagasts',
		'Jaunjelgavas pagasts',
		'Jaunjelgavas pilsēta',
		'Seces pagasts',
		'Sērenes pagasts',
		'Staburaga pagasts',
		'Sunākstes pagasts'
	),
	'Jaunpiebalgas novads' => array(
		'Jaunpiebalgas pagasts',
		'Zosēnu pagasts'
	),
	'Jaunpils novads' => array(
		'Jaunpils pagasts',
		'Viesatu pagasts'
	),
	'Jēkabpils novads' => array(
		'Ābeļu pagasts',
		'Dignājas pagasts',
		'Dunavas pagasts',
		'Kalna pagasts',
		'Leimaņu pagasts',
		'Rubenes pagasts',
		'Zasas pagasts'
	),
	'Jelgavas novads' => array(
		'Elejas pagasts',
		'Glūdas pagasts',
		'Jaunsvirlaukas pagasts',
		'Kalnciema pagasts',
		'Lielplatones pagasts',
		'Līvbērzes pagasts',
		'Platones pagasts',
		'Sesavas pagasts',
		'Svētes pagasts',
		'Valgundes pagasts',
		'Vilces pagasts',
		'Vircavas pagasts',
		'Zaļenieku pagasts'
	),
	'Kandavas novads' => array(
		'Cēres pagasts',
		'Kandavas pagasts',
		'Kandavas pilsēta',
		'Matkules pagasts',
		'Vānes pagasts',
		'Zantes pagasts',
		'Zemītes pagasts'
	),
	'Kārsavas novads' => array(
		'Goliševas pagasts',
		'Kārsavas pilsēta',
		'Malnavas pagasts',
		'Mērdzenes pagasts',
		'Mežvidu pagasts',
		'Salnavas pagasts'
	),
	'Kocēnu novads' => array(
		'Bērzaines pagasts',
		'Dikļu pagasts',
		'Kocēnu pagasts',
		'Vaidavas pagasts',
		'Zilākalna pagasts'
	),
	'Kokneses novads' => array(
		'Bebru pagasts',
		'Iršu pagasts',
		'Kokneses pagasts'
	),
	'Krāslavas novads' => array(
		'Aulejas pagasts',
		'Indras pagasts',
		'Izvaltas pagasts',
		'Kalniešu pagasts',
		'Kaplavas pagasts',
		'Kombuļu pagasts',
		'Krāslavas pagasts',
		'Krāslavas pilsēta',
		'Piedrujas pagasts',
		'Robežnieku pagasts',
		'Skaistas pagasts',
		'Ūdrīšu pagasts'
	),
	'Krimuldas novads' => array(
		'Krimuldas pagasts',
		'Lēdurgas pagasts'
	),
	'Krustpils novads' => array(
		'Atašienes pagasts',
		'Krustpils pagasts',
		'Kūku pagasts',
		'Mežāres pagasts',
		'Variešu pagasts',
		'Vīpes pagasts'
	),
	'Kuldīgas novads' => array(
		'Ēdoles pagasts',
		'Gudenieku pagasts',
		'Īvandes pagasts',
		'Kabiles pagasts',
		'Kuldīgas pilsēta',
		'Kurmāles pagasts',
		'Laidu pagasts',
		'Padures pagasts',
		'Pelču pagasts',
		'Rendas pagasts',
		'Rumbas pagasts',
		'Snēpeles pagasts',
		'Turlavas pagasts',
		'Vārmes pagasts'
	),
	'Ķeguma novads' => array(
		'Birzgales pagasts',
		'Ķeguma pilsēta',
		'Rembates pagasts',
		'Tomes pagasts'
	),
	'Ķekavas novads' => array(
		'Baložu pilsēta',
		'Daugmales pagasts',
		'Ķekavas pagasts'
	),
	'Lielvārdes novads' => array(
		'Jumpravas pagasts',
		'Lēdmanes pagasts',
		'Lielvārdes pagasts',
		'Lielvārdes pilsēta'
	),
	'Līgatnes novads' => array(
		'Līgatnes pagasts',
		'Līgatnes pilsēta'
	),
	'Limbažu novads' => array(
		'Katvaru pagasts',
		'Limbažu pagasts',
		'Limbažu pilsēta',
		'Pāles pagasts',
		'Skultes pagasts',
		'Umurgas pagasts',
		'Vidrižu pagasts',
		'Viļķenes pagasts'
	),
	'Līvānu novads' => array(
		'Jersikas pagasts',
		'Līvānu pilsēta',
		'Rožupes pagasts',
		'Rudzātu pagasts',
		'Sutru pagasts',
		'Turku pagasts'
	),
	'Lubānas novads' => array(
		'Indrānu pagasts',
		'Lubānas pilsēta'
	),
	'Ludzas novads' => array(
		'Briģu pagasts',
		'Cirmas pagasts',
		'Isnaudas pagasts',
		'Istras pagasts',
		'Ludzas pilsēta',
		'Nirzas pagasts',
		'Ņukšu pagasts',
		'Pildas pagasts',
		'Pureņu pagasts',
		'Rundēnu pagasts'
	),
	'Madonas novads' => array(
		'Aronas pagasts',
		'Barkavas pagasts',
		'Bērzaunes pagasts',
		'Dzelzavas pagasts',
		'Kalsnavas pagasts',
		'Lazdonas pagasts',
		'Liezēres pagasts',
		'Ļaudonas pagasts',
		'Madonas pilsēta',
		'Mārcienas pagasts',
		'Mētrienas pagasts',
		'Ošupes pagasts',
		'Praulienas pagasts',
		'Sarkaņu pagasts',
		'Vestienas pagasts'
	),
	'Mālpils novads' => array(),
	'Mārupes novads' => array(),
	'Mazsalacas novads' => array(
		'Mazsalacas pagasts',
		'Mazsalacas pilsēta',
		'Ramatas pagasts',
		'Sēļu pagasts',
		'Skaņkalnes pagasts'
	),
	'Mērsraga novads' => array(),
	'Naukšēnu novads' => array(
		'Ķoņu pagasts',
		'Naukšēnu pagasts'
	),
	'Neretas novads' => array(
		'Mazzalves pagasts',
		'Neretas pagasts',
		'Pilskalnes pagasts',
		'Zalves pagasts'
	),
	'Nīcas novads' => array(
		'Nīcas pagasts',
		'Otaņķu pagasts'
	),
	'Ogres novads' => array(
		'Krapes pagasts',
		'Ķeipenes pagasts',
		'Lauberes pagasts',
		'Madlienas pagasts',
		'Mazozolu pagasts',
		'Meņģeles pagasts',
		'Ogresgala pagasts',
		'Ogres pilsēta',
		'Suntažu pagasts',
		'Taurupes pagasts'
	),
	'Olaines novads' => array(
		'Olaines pagasts',
		'Olaines pilsēta'
	),
	'Ozolnieku novads' => array(
		'Cenu pagasts',
		'Ozolnieku pagasts',
		'Salgales pagasts'
	),
	'Pārgaujas novads' => array(
		'Raiskuma pagasts',
		'Stalbes pagasts',
		'Straupes pagasts'
	),
	'Pāvilostas novads' => array(
		'Pāvilostas pilsēta',
		'Sakas pagasts',
		'Vērgales pagasts'
	),
	'Pļaviņu novads' => array(
		'Aiviekstes pagasts',
		'Klintaines pagasts',
		'Pļaviņu pilsēta',
		'Vietalvas pagasts'
	),
	'Preiļu novads' => array(
		'Aizkalnes pagasts',
		'Pelēču pagasts',
		'Preiļu pagasts',
		'Preiļu pilsēta',
		'Saunas pagasts'
	),
	'Priekules novads' => array(
		'Bunkas pagasts',
		'Gramzdas pagasts',
		'Kalētu pagasts',
		'Priekules pagasts',
		'Priekules pilsēta',
		'Virgas pagasts'
	),
	'Priekuļu novads' => array(
		'Liepas pagasts',
		'Mārsnēnu pagasts',
		'Priekuļu pagasts',
		'Veselavas pagasts'
	),
	'Raunas novads' => array(
		'Drustu pagasts',
		'Raunas pagasts'
	),
	'Rēzeknes novads' => array(
		'Audriņu pagasts',
		'Bērzgales pagasts',
		'Čornajas pagasts',
		'Dricānu pagasts',
		'Feimaņu pagasts',
		'Gaigalavas pagasts',
		'Griškānu pagasts',
		'Ilzeskalna pagasts',
		'Kantinieku pagasts',
		'Kaunatas pagasts',
		'Lendžu pagasts',
		'Lūznavas pagasts',
		'Mākoņkalna pagasts',
		'Maltas pagasts',
		'Nagļu pagasts',
		'Nautrēnu pagasts',
		'Ozolaines pagasts',
		'Ozolmuižas pagasts',
		'Pušas pagasts',
		'Rikavas pagasts',
		'Sakstagala pagasts',
		'Silmalas pagasts',
		'Stoļerovas pagasts',
		'Stružānu pagasts',
		'Vērēmu pagasts'
	),
	'Riebiņu novads' => array(
		'Galēnu pagasts',
		'Riebiņu pagasts',
		'Rušonas pagasts',
		'Silajāņu pagasts',
		'Sīļukalna pagasts',
		'Stabulnieku pagasts'
	),
	'Rojas novads' => array(),
	'Ropažu novads' => array(),
	'Rucavas novads' => array(
		'Dunikas pagasts',
		'Rucavas pagasts'
	),
	'Rugāju novads' => array(
		'Lazdukalna pagasts',
		'Rugāju pagasts'
	),
	'Rūjienas novads' => array(
		'Ipiķu pagasts',
		'Jeru pagasts',
		'Lodes pagasts',
		'Rūjienas pilsēta',
		'Vilpulkas pagasts'
	),
	'Rundāles novads' => array(
		'Rundāles pagasts',
		'Svitenes pagasts',
		'Viesturu pagasts'
	),
	'Salacgrīvas novads' => array(
		'Ainažu pagasts',
		'Ainažu pilsēta',
		'Liepupes pagasts',
		'Salacgrīvas pagasts',
		'Salacgrīvas pilsēta'
	),
	'Salas novads' => array(
		'Salas pagasts',
		'Sēlpils pagasts'
	),
	'Salaspils novads' => array(
		'Salaspils pagasts',
		'Salaspils pilsēta'
	),
	'Saldus novads' => array(
		'Ezeres pagasts',
		'Jaunauces pagasts',
		'Jaunlutriņu pagasts',
		'Kursīšu pagasts',
		'Lutriņu pagasts',
		'Nīgrandes pagasts',
		'Novadnieku pagasts',
		'Pampāļu pagasts',
		'Rubas pagasts',
		'Saldus pagasts',
		'Saldus pilsēta',
		'Šķēdes pagasts',
		'Vadakstes pagasts',
		'Zaņas pagasts',
		'Zirņu pagasts',
		'Zvārdes pagasts'
	),
	'Saulkrastu novads' => array(
		'Saulkrastu pagasts',
		'Saulkrastu pilsēta'
	),
	'Sējas novads' => array(),
	'Siguldas novads' => array(
		'Allažu pagasts',
		'Mores pagasts',
		'Siguldas pagasts',
		'Siguldas pilsēta'
	),
	'Skrīveru novads' => array(),
	'Skrundas novads' => array(
		'Nīkrāces pagasts',
		'Raņķu pagasts',
		'Rudbāržu pagasts',
		'Skrundas pagasts',
		'Skrundas pilsēta'
	),
	'Smiltenes novads' => array(
		'Bilskas pagasts',
		'Blomes pagasts',
		'Brantu pagasts',
		'Grundzāles pagasts',
		'Launkalnes pagasts',
		'Palsmanes pagasts',
		'Smiltenes pagasts',
		'Smiltenes pilsēta',
		'Variņu pagasts'
	),
	'Stopiņu novads' => array(),
	'Strenču novads' => array(
		'Jērcēnu pagasts',
		'Plāņu pagasts',
		'Sedas pilsēta',
		'Strenču pilsēta'
	),
	'Talsu novads' => array(
		'Abavas pagasts',
		'Ārlavas pagasts',
		'Balgales pagasts',
		'Ģibuļu pagasts',
		'Īves pagasts',
		'Ķūļciema pagasts',
		'Laidzes pagasts',
		'Laucienes pagasts',
		'Lībagu pagasts',
		'Lubes pagasts',
		'Sabiles pilsēta',
		'Stendes pilsēta',
		'Strazdes pagasts',
		'Talsu pilsēta',
		'Valdemārpils pilsēta',
		'Valdgales pagasts',
		'Vandzenes pagasts',
		'Virbu pagasts'
	),
	'Tērvetes novads' => array(
		'Augstkalnes pagasts',
		'Bukaišu pagasts',
		'Tērvetes pagasts'
	),
	'Tukuma novads' => array(
		'Degoles pagasts',
		'Džūkstes pagasts',
		'Irlavas pagasts',
		'Jaunsātu pagasts',
		'Lestenes pagasts',
		'Pūres pagasts',
		'Sēmes pagasts',
		'Slampes pagasts',
		'Tukuma pilsēta',
		'Tumes pagasts',
		'Zentenes pagasts'
	),
	'Vaiņodes novads' => array(
		'Embūtes pagasts',
		'Vaiņodes pagasts'
	),
	'Valkas novads' => array(
		'Ērģemes pagasts',
		'Kārķu pagasts',
		'Valkas pagasts',
		'Valkas pilsēta',
		'Vijciema pagasts',
		'Zvārtavas pagasts'
	),
	'Varakļānu novads' => array(
		'Murmastienes pagasts',
		'Varakļānu pagasts',
		'Varakļānu pilsēta'
	),
	'Vārkavas novads' => array(
		'Rožkalnu pagasts',
		'Upmalas pagasts',
		'Vārkavas pagasts'
	),
	'Vecpiebalgas novads' => array(
		'Dzērbenes pagasts',
		'Inešu pagasts',
		'Kaives pagasts',
		'Taurenes pagasts',
		'Vecpiebalgas pagasts'
	),
	'Vecumnieku novads' => array(
		'Bārbeles pagasts',
		'Kurmenes pagasts',
		'Skaistkalnes pagasts',
		'Stelpes pagasts',
		'Valles pagasts',
		'Vecumnieku pagasts'
	),
	'Ventspils novads' => array(
		'Ances pagasts',
		'Jūrkalnes pagasts',
		'Piltenes pagasts',
		'Piltenes pilsēta',
		'Popes pagasts',
		'Puzes pagasts',
		'Tārgales pagasts',
		'Ugāles pagasts',
		'Usmas pagasts',
		'Užavas pagasts',
		'Vārves pagasts',
		'Ziru pagasts',
		'Zlēku pagasts'
	),
	'Viesītes novads' => array(
		'Elkšņu pagasts',
		'Rites pagasts',
		'Saukas pagasts',
		'Viesītes pagasts',
		'Viesītes pilsēta'
	),
	'Viļakas novads' => array(
		'Kupravas pagasts',
		'Medņevas pagasts',
		'Susāju pagasts',
		'Šķilbēnu pagasts',
		'Vecumu pagasts',
		'Viļakas pilsēta',
		'Žīguru pagasts'
	),
	'Viļānu novads' => array(
		'Dekšāres pagasts',
		'Sokolku pagasts',
		'Viļānu pagasts',
		'Viļānu pilsēta'
	),
	'Zilupes novads' => array(
		'Lauderu pagasts',
		'Pasienes pagasts',
		'Zaļesjes pagasts',
		'Zilupes pilsēta'
	)
);



